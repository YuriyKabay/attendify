This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

To run project:
### `npm run build`
### `serve -s build`

Or:
### `npm start`

Mocks are available in MOCK_DATA.csv file