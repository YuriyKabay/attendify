import React, { memo } from 'react';

const getSum = (values, headers) => {
  return values.reduce((total, item) => {
    if (Object.is(Number(item[headers[1]]), NaN)) {
      return total;
    }
    return total + Number(item[headers[1]]);
  }, 0);
};

const Results = ({ map, headers }) => {
  if (!map) {
    return null;
  }
  const values = Object.values(map);
  const sum = getSum(values, headers);
  const average = sum / values.length;

  return (
    <div className='col centered-content'>
      <div>
        <div>Sum: {sum}</div>
        <div>Avg: {average}</div>
      </div>
    </div>
  )
};

export default memo(Results);
