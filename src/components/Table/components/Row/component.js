import React, { PureComponent } from 'react';
import Input from "../Input";

class Row extends PureComponent {
  handleChange = (value, header) => {
    const { orderKey, handleChange } = this.props;
    handleChange(value, orderKey, header);
  };

  render() {
    const { row, headers } = this.props;

    return (
      <tr>
        <td>
          <Input
            type="text"
            value={row[headers[0]]}
            header={headers[0]}
            handleChange={this.handleChange}
          />
        </td>
        <td>
          <Input
            type="number"
            value={row[headers[1]]}
            header={headers[1]}
            handleChange={this.handleChange}
          />
        </td>
      </tr>
    )
  }
}

export default Row;
