import React, { PureComponent } from 'react';
import Input from "../Input";

class HeaderRow extends PureComponent {
  handleChange = (value, header) => {
    const { handleChange } = this.props;
    handleChange(value, 0, header);
  };

  render() {
    const { map, headers } = this.props;

    return (
      <tr>
        <th>
          <Input
            type="text"
            value={map[0][headers[0]]}
            header={headers[0]}
            handleChange={this.handleChange}
          />
        </th>
        <th>
          <Input
            type="text"
            value={map[0][headers[1]]}
            header={headers[1]}
            handleChange={this.handleChange}
          />
        </th>
      </tr>
    )
  }
}

export default HeaderRow;
