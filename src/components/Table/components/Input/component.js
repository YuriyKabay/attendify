import React, { PureComponent } from 'react';
import './style.css'

class Input extends PureComponent {
  handleChange = ev => {
    const { handleChange, header } = this.props;
    handleChange(ev.target.value, header);
  };

  render() {
    const { value, type } = this.props;

    return (
      <input
        type={type}
        value={value}
        onChange={this.handleChange}
      />
    )
  }
}

export default Input;
