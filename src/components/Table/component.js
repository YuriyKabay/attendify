import React, { memo } from 'react';
import './style.css';
import Row from "./components/Row";
import HeaderRow from "./components/HeaderRow";

const Table = ({ map, order, headers, handleChange }) => {
  if (!map) { return null; }

  return (
    <div className='col'>
      <table>
        <thead>
          <HeaderRow
            map={map}
            headers={headers}
            handleChange={handleChange}
          />
        </thead>
        <tbody>
        {order.map((orderKey) => (
          <Row
            key={orderKey}
            orderKey={orderKey}
            row={map[orderKey]}
            headers={headers}
            handleChange={handleChange}
          />
        ))}
        </tbody>
      </table>
    </div>
  )
};

export default memo(Table);
