import React, { PureComponent } from 'react';
import CSVReader from 'react-csv-reader'

class FileUploader extends PureComponent {
  state = {
    label: 'Upload file'
  };

  static isDataValid(data) {
    if (!Array.isArray(data)) { return false; }
    if (data[0].length !== 2) { return false; }
    return true;
  }

  static createNormalizedStructure(data) {
    const headers = [data[0][0], data[0][1]];
    const order = [];
    const map = {
      0: { [data[0][0]]: data[0][0], [data[0][1]]: data[0][1] }
    };

    for(let i = 1; i < data.length; i++) {
      map[i] = {
        [data[0][0]]: data[i][0],
        [data[0][1]]: data[i][1],
      };
      order.push(i);
    }

    return { map, order, headers };
  }

  handleUpload = (data) => {
    if (!FileUploader.isDataValid(data)) {
      this.setState({ label: 'Invalid structure of file' });
      return;
    }

    const { map, order, headers } = FileUploader.createNormalizedStructure(data);
    this.props.handleUpload(map, order, headers);
    this.setState({ label: 'Upload file' });
  };

  handleError = () => {
    this.setState({ label: 'An error occurred. Please, try again.' });
  };

  render() {
    const { label } = this.state;

    return (
      <div className='col centered-content'>
        <CSVReader
          cssClass="csv-input"
          label={label}
          onFileLoaded={this.handleUpload}
          onError={this.handleError}
        />
      </div>
    )
  }
}

export default FileUploader;
