import React, { PureComponent } from 'react';

import FileUploader from '../FileUploader';
import Table from '../Table';
import Results from '../Results';

class App extends PureComponent {
  state = {
    map: undefined,
    order: undefined,
    headers: undefined
  };

  handleChange = (value, orderKey, headerKey) => {
    this.setState({
      map: {
        ...this.state.map,
        [orderKey]: {
          ...this.state.map[orderKey],
          [headerKey]: value
        }
      }
    });
  };

  handleUpload = (map, order, headers) => {
    this.setState({ map, order, headers });
  };

  render() {
    const { map, order, headers } = this.state;

    return (
      <div className='row app'>
        <FileUploader handleUpload={this.handleUpload} />
        <Table map={map} order={order} headers={headers} handleChange={this.handleChange} />
        <Results map={map} headers={headers} />
      </div>
    )
  }
}

export default App;
